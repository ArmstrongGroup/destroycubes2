﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolEnemyManager : MonoBehaviour
{
    public GameObject enemyPrefab;
    public GameObject enemyParticlePrefab;
    public int numberOfEnemies = 1;
    public float firstSpawnTimeWave = 4;
    public float maxSpawnGapWave = 4;
    public float minSpawnGapWave = 0.5f;
    public float firstSpawnTime = 4;
    public float maxSpawnGap = 4;
    public float minSpawnGap = 0.5f;
    public AnimationCurve spawnPattern;
    public AnimationCurve waveSpawnPattern;

    public PatrolEnemyController[] enemies;
    public ParticleSystem[] patrolParticles;

    float nextSpawn;
    float nextWaveSpawn;
    float startTime = 5f;

    bool spawnVert = false;
    bool spawnWaveVert = false;


    void OnEnable()
    {
        GameStateMachine.OnEnterGame += SetStartTime;
        GameStateMachine.OnEnterGame += StartSpawnTime;
        GameStateMachine.OnStartScreen += StopParticle;
        PlayerController.OnResetAfterDeath += StopParticle;
        PlayerController.OnGameOver += PauseParticle;
        PlayerController.OnLostLife += UnPauseParticle;
    }

    void OnDisable()
    {
        GameStateMachine.OnEnterGame -= SetStartTime;
        GameStateMachine.OnEnterGame -= StartSpawnTime;
        GameStateMachine.OnStartScreen -= StopParticle;
        PlayerController.OnResetAfterDeath -= StopParticle;
        PlayerController.OnGameOver -= PauseParticle;
        PlayerController.OnLostLife -= UnPauseParticle;
    }

    // Start is called before the first frame update
    void Awake()
    {
        Physics.IgnoreLayerCollision(8, 8);
        CreateEnemies();
        CreateParticles();
    }

    void CreateEnemies()
    {
        enemies = new PatrolEnemyController[numberOfEnemies];
        for (int i = 0; i < enemies.Length; i++)
        {
            enemies[i] = Instantiate(enemyPrefab).GetComponent<PatrolEnemyController>();
            enemies[i].gameObject.transform.position = new Vector3(-12, 0, -12);
        }
    }

    void CreateParticles()
    {
        patrolParticles = new ParticleSystem[numberOfEnemies];
        for (int i = 0; i < patrolParticles.Length; i++)
        {
            patrolParticles[i] = Instantiate(enemyParticlePrefab).GetComponent<ParticleSystem>();
            patrolParticles[i].gameObject.transform.position = new Vector3(0, 0, -12);
        }
    }

    void WaveSpawn()
    {
        if (IsNextWaveSpawn())
        {
            Vector3 wavePos = PatrolGenerateWavePos();
            int count = 6;
            for (int i = 0; i < count; i++)
            {
                if (enemies[i].IsSpawnable())
                {
                    if (spawnWaveVert)
                    {
                        wavePos = wavePos + new Vector3(1, 0, 0);
                        enemies[i].currentPatrolEnemyType = PatrolEnemyController.PatrolEnemyType.vertical;
                    }
                    else
                    {
                        wavePos = wavePos + new Vector3(0, 0, 1);
                        enemies[i].currentPatrolEnemyType = PatrolEnemyController.PatrolEnemyType.horizontal;
                    }
                    patrolParticles[i].gameObject.transform.position = wavePos;
                    patrolParticles[i].Play();
                    enemies[i].PatrolSpawn(wavePos);
                }
                else 
                {
                    count += 1;
                    return;
                }
            }
        }
    }

    void Spawn()
    {
        if (IsNextSpawn())
        {
            for (int i = 0; i < enemies.Length; i++)
            {
                if (enemies[i].IsSpawnable())
                {
                    Vector3 pos = PatrolGeneratePos();
                    //Vector3 pos = TrackerGeneratePos();
                    patrolParticles[i].gameObject.transform.position = pos;
                    patrolParticles[i].Play();
                    if (spawnVert)
                    {
                        enemies[i].currentPatrolEnemyType = PatrolEnemyController.PatrolEnemyType.vertical;
                    }
                    else
                    {
                        enemies[i].currentPatrolEnemyType = PatrolEnemyController.PatrolEnemyType.horizontal;
                    }
                    enemies[i].PatrolSpawn(pos);
                    
                    break;
                }
            }
        }
    }

    void PauseParticle()
    {
        foreach (ParticleSystem pSys in patrolParticles)
        {
            pSys.Pause();
        }
    }

    void UnPauseParticle()
    {
        foreach (ParticleSystem pSys in patrolParticles)
        {
            pSys.Pause();
        }
    }

    void StopParticle()
    {
        foreach (ParticleSystem pSys in patrolParticles)
        {
            pSys.Stop();
            pSys.Clear();
        }
    }

    Vector3 PatrolGeneratePos()
    {
        int side = Random.Range(1, 5);
        Vector3 loc = Vector3.zero;
        switch (side)
        {
            case 1:
                loc = new Vector3(Random.Range(-8, 7) + 0.5f, 0, 4f);
                spawnVert = true;
                break;
            case 2:
                loc = new Vector3(7.5f, 0, Random.Range(-4, 4));
                spawnVert = false;
                break;
            case 3:
                loc = new Vector3(Random.Range(-8, 7) + 0.5f, 0, -4f);
                spawnVert = true;
                break;
            case 4:
                loc = new Vector3(-7.5f, 0, Random.Range(-4, 4) );
                spawnVert = false;
                break;
            default:
                //Should never be seen, but just in case
                loc = new Vector3(-7.5f, 0, Random.Range(-4, 4));
                spawnVert = false;
                break;
        }
        return loc;
    }

    Vector3 PatrolGenerateWavePos()
    {
        int side = Random.Range(1, 5);
        Vector3 loc = Vector3.zero;
        switch (side)
        {
            case 1:
                loc = new Vector3(Random.Range(-8, 2) + 0.5f, 0, 4f);
                spawnWaveVert = true;
                break;
            case 2:
                loc = new Vector3(7.5f, 0, Random.Range(-4, -1) );
                spawnWaveVert = false;
                break;
            case 3:
                loc = new Vector3(Random.Range(-8, 2) + 0.5f, 0, -4f);
                spawnWaveVert = true;
                break;
            case 4:
                loc = new Vector3(-7.5f, 0, Random.Range(-4, -1));
                spawnWaveVert = false;
                break;
            default:
                //Should never be seen, but just in case
                loc = new Vector3(-7.5f, 0, Random.Range(-4, 1));
                spawnWaveVert = false;
                break;
        }
        return loc;
    }

    void SetStartTime()
    {
        startTime = Time.time;
    }

    float GenerateNextSpawnTime(AnimationCurve curve)
    {
        float rand = Random.Range(0.0f, 5.0f);
        float time = curve.Evaluate(Time.time - startTime + rand) * maxSpawnGapWave;
        /*if (time < minSpawnGapWave)
        {
            time = minSpawnGapWave;
        }*/
        return time;
    }

    // Update is called once per frame
    public void EnemiesUpdate()
    {
        RunEnemies();
        Spawn();
        WaveSpawn();
    }

    void RunEnemies()
    {
        foreach (PatrolEnemyController enemy in enemies)
        {
            enemy.EnemyUpdate();
        }
    }

    bool IsNextSpawn()
    {
        if (Time.time > nextSpawn )
        {
            nextSpawn = Time.time + (GenerateNextSpawnTime(spawnPattern)* maxSpawnGap);
            if (nextSpawn < minSpawnGap)
            {
                nextSpawn = minSpawnGap;
            }
            return true;
        }
        return false;
    }

    bool IsNextWaveSpawn()
    {
        if (Time.time > nextWaveSpawn )
        {
            nextWaveSpawn = Time.time + (GenerateNextSpawnTime(waveSpawnPattern) * maxSpawnGapWave);
            if (nextWaveSpawn < minSpawnGapWave)
            {
                nextWaveSpawn = minSpawnGapWave;
            }
            return true;
        }
        return false;
    }

    void StartSpawnTime()
    {
        nextSpawn = Time.time + firstSpawnTime;
        nextWaveSpawn = Time.time + firstSpawnTimeWave;
    }
}
