﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HazardManager : MonoBehaviour
{
    public GameObject hazardSpawnerPrefab;
    public float firstSpawnTime = 4;
    public float maxSpawnGap = 4;
    public float minSpawnGap = 0.5f;
    public AnimationCurve spawnPattern;

    public HazardController[] hazards;
    GameObject hazardSpawner;
    HazardSpawnMotion hazardSpawnerMotion;

    float nextSpawn;
    float spawnInterval = 10f;
    float endSpawnPattern;
    bool isSpawnActive;
    float startTime;

    bool standardSpawn;

    // Start is called before the first frame update
    void Awake()
    {
        Physics.IgnoreLayerCollision(10, 10);
        hazardSpawner = Instantiate(hazardSpawnerPrefab);
        hazardSpawnerMotion = hazardSpawner.GetComponent<HazardSpawnMotion>();
        hazardSpawner.transform.position = new Vector3(-12, 0, -12);
    }

    void OnEnable()
    {
        GameStateMachine.OnEnterGame += SetPlayStartTime;

        PlayerController.OnLostLife += SetNextSpawnTime;
        PlayerController.OnResetAfterDeath += SetNextSpawnTime;
    }

    void OnDisable()
    {
        GameStateMachine.OnEnterGame -= SetPlayStartTime;

        PlayerController.OnLostLife -= SetNextSpawnTime;
        PlayerController.OnResetAfterDeath -= SetNextSpawnTime;
    }

    float GenerateNextSpawnTime()
    {
        float time = spawnPattern.Evaluate(Time.time - startTime) * maxSpawnGap;
        if (time < minSpawnGap) 
        {
            time = minSpawnGap;
        }return time;
    }

    void SetPlayStartTime()
    {
        startTime = Time.time;
        //SetNextSpawnTime();
        nextSpawn = startTime + firstSpawnTime;
    }

    void SetNextSpawnTime()
    {
        nextSpawn = Time.time + GenerateNextSpawnTime();
    }

    public void HazardsUpdate()
    {
        Spawn();
        hazardSpawnerMotion.HazardSpawnUpdate();
    }

    void Spawn()
    {
        if (IsNextSpawn())
        {
            PlaceSpawner();
        }
        else if (isSpawnActive)
        {
            EndSpawnActive();
        }
    }

    void PlaceSpawner()
    {
        Vector3 loc = Vector3.zero;
        float rot = 0;
        int side = Random.Range(1, 5);
        
        switch (side)
        {
            case 1:
                loc = new Vector3(Random.Range(-8, 8) + 0.5f, 0, 5f);
                rot = 180;
                //Debug.Log("Top");
                break;
            case 2:
                loc = new Vector3(8.5f, 0, Random.Range(-5, 5));
                rot = -90;
                //Debug.Log("Right");
                break;
            case 3:
                loc = new Vector3(Random.Range(-8, 8) + 0.5f, 0, -5);
                //Debug.Log("Bottom");
                break;
            case 4:
                loc = new Vector3(-8.5f, 0, Random.Range(-5, 5));
                rot = 90;
                //Debug.Log("Left");
                break;
            default:
                //Should never be seen, but just in case
                rot = 90;
                loc = new Vector3(-8.5f, 0, Random.Range(-5, 5));
                break;
        }

        hazardSpawner.transform.position = loc;
        hazardSpawnerMotion.SpawnStart();
        hazardSpawner.transform.eulerAngles = new Vector3(0, rot, 0);
        hazardSpawnerMotion.IsActive = true;
    }

    bool IsNextSpawn()
    {
        if (Time.time > nextSpawn)
        {
            nextSpawn = Time.time + GenerateNextSpawnTime();
            endSpawnPattern = nextSpawn - 5;
            isSpawnActive = true;
            return true;
        }
        return false;
    }

    void EndSpawnActive()
    {
        if (Time.time > endSpawnPattern)
        {
            nextSpawn = Time.time + spawnInterval;
            //nextSpawn = Time.time + +GenerateNextSpawnTime();
            hazardSpawnerMotion.StopSpawner();
            isSpawnActive = false;
        }
    }
}
