﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SetRawImageSize : MonoBehaviour
{
    [SerializeField]
    RenderTexture lowResImage;

    [SerializeField]
    RectTransform lowResTran;

    [SerializeField]
    int lowResWidth = 235;

    [SerializeField]
    RawImage rawImage;

    CanvasScaler canvas;

    //float screenX;
    //float screenZ;
    float ratio;
    int lowResHeight = 0;

    // Start is called before the first frame update
    void Start()
    {
        canvas = GameObject.Find("Canvas").GetComponent<CanvasScaler>();
        GetScreenSize();
        SetImageSize();

        Debug.Log(ratio);
        Debug.Log(lowResHeight);
        Debug.Log(canvas.referenceResolution);
        Debug.Log(lowResImage.height);
        Debug.Log(lowResImage.width);

        SceneManager.LoadScene(1);
    }

    void GetScreenSize()
    {
        Vector3 ScreenSize = new Vector3(Screen.width, Screen.height, 0);
        Vector3 ScreenEdge = Camera.main.ScreenToWorldPoint(ScreenSize);
        //screenX = ScreenEdge.x;
        //screenZ = ScreenEdge.z;
        ratio = ScreenEdge.x / ScreenEdge.z;
    }

    void SetImageSize()
    {
        lowResHeight = Mathf.RoundToInt(lowResWidth / ratio);
   
        canvas.referenceResolution = new Vector2(lowResWidth, lowResHeight);

        lowResTran.sizeDelta = (new Vector2(lowResWidth, lowResHeight));

        lowResImage.Release();
        lowResImage.width = lowResWidth;
        lowResImage.height = lowResHeight;

        rawImage.SetNativeSize();
    }

    // Update is called once per frame
    void Update()
    {
        //SetImageSize();
    }
}
