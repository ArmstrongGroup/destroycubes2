﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Transitions from one state to another
public class GameStateMachine : MonoBehaviour
{
    public GameObject startScreen;
    public GameObject gameOverScreen;
    public GameObject playScreen;
    public GameObject scoreboard;

    public delegate void EnterStartScreen();
    public delegate void EnterScoreboard();
    public delegate void EnterGame();
    public delegate void ButtonPress();

    public static event EnterStartScreen OnStartScreen;
    public static event EnterScoreboard OnScoreboard;
    public static event EnterGame OnEnterGame;
    public static event EnterGame OnButtonPress;

    enum GameState {playing, menus};
    GameState currentGameState = GameState.menus;

    PlayerController _pController;
    PlayerCollisions _pCollisions;
    EnemyManager _eManager;
    TrackerEnemyManager _tEManager;
    PatrolEnemyManager _pEManager;
    HazardManager _hManager;
    TimeManager _tManager;

    void OnEnable()
    {
        PlayerController.OnGameOver += GameOver;
    }


    void OnDisable()
    {
        PlayerController.OnGameOver -= GameOver;
    }


    // Start is called before the first frame update
    void Start()
    {
        Application.targetFrameRate = 60;

        _pController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        _pCollisions = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerCollisions>();
        _eManager = GetComponent<EnemyManager>();
        _tEManager = GetComponent<TrackerEnemyManager>();
        _pEManager = GetComponent<PatrolEnemyManager>();
        _hManager = GetComponent<HazardManager>();
        _tManager = GetComponent<TimeManager>();
        startScreen.SetActive(true);
        gameOverScreen.SetActive(false);
        playScreen.SetActive(false);
        scoreboard.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        GameStateUpdate();
    }

    void GameStateUpdate()
    {
        switch (currentGameState)
        { 
            case GameState.playing:
                _pController.PlayerUpdate();
                _eManager.EnemiesUpdate();
                _tEManager.EnemiesUpdate();
                _pEManager.EnemiesUpdate();
                _hManager.HazardsUpdate();
                _pCollisions.PlayerCollisionsUpdate();
                _tManager.TimeUpdate();
            break;

            case GameState.menus:

            break;

            default:
                Debug.LogError("Invalid Game State");
            break;
        }
    }

    void GameOver()
    {
        startScreen.SetActive(false);
        currentGameState = GameState.menus;
        gameOverScreen.SetActive(true);
        playScreen.SetActive(false);
        scoreboard.SetActive(false);
    }

    //Functions for menus
    public void Play()
    {
        OnEnterGame?.Invoke();
        OnButtonPress?.Invoke();
        currentGameState = GameState.playing;
        startScreen.SetActive(false);
        gameOverScreen.SetActive(false); 
        playScreen.SetActive(true); 
        scoreboard.SetActive(false);
    }

    public void Scoreboard()
    {
        OnScoreboard?.Invoke();
        OnButtonPress?.Invoke();
        startScreen.SetActive(false);
        gameOverScreen.SetActive(false);
        playScreen.SetActive(false);
        scoreboard.SetActive(true);
    }

    public void StartScreen()
    {
        OnStartScreen?.Invoke();
        OnButtonPress?.Invoke();
        startScreen.SetActive(true);
        gameOverScreen.SetActive(false);
        playScreen.SetActive(false);
        scoreboard.SetActive(false);
    }
}
