﻿using System.Collections;
using UnityEngine;
using UnityEngine.Advertisements;

public class AdManager : MonoBehaviour
{
    //TODO currently broken and needs fixing.

    //Can only implament one ad. Need to use js or objective c to have 2 (platform specific).
    string gameId = "3586021"; //Google Play ID
    string placementId_Top = "bannerAd_Top";
    //string placementId_Bottom = "bannerAd_Bottom";
    public bool testMode = true;
    IUnityAdsInitializationListener listener;

    void OnEnable()
    {
        //GameStateMachine.OnEnterGame += ClearAd;
        //GameStateMachine.OnStartScreen += BannerTop;
        //PlayerController.OnGameOver += BannerTop;
    }

    void OnDisable()
    {
        //GameStateMachine.OnEnterGame -= ClearAd;
        //GameStateMachine.OnStartScreen -= BannerTop;
        //PlayerController.OnGameOver -= BannerTop;
    }

    void Awake()
    {
        //Advertisement.Initialize(gameId, testMode, listener);
        //Advertisement.Load(placementId_Top);
        //ShowBannerTop();
    }

    void ShowBannerTop()
    {
        if (Advertisement.isShowing)
        {
            Advertisement.Banner.SetPosition(BannerPosition.TOP_CENTER);
            Advertisement.Banner.Show(placementId_Top);

            //Debug.Log("ShowAd_Top");
        }
        else
        {
            Invoke("ShowBannerTop", 0.2f);
        }
    }

    /*void ShowBannerBottom()
    {
        if (Advertisement.IsReady(placementId_Bottom))
        {
            
            Advertisement.Banner.Show(placementId_Bottom);

            Debug.Log("ShowAd_Bottom");
        }
        else
        {
            Invoke("ShowBannerBottom", 0.2f);
        }
    }

    void BannerBottom()
    {
        ClearAd();
        Advertisement.Load(placementId_Bottom);
        ShowBannerBottom();
    }*/

    void BannerTop()
    {
        ClearAd();
        //Advertisement.Load(placementId_Top);
        Invoke("BannerAd", 0.5f);
    }

    void BannerAd()
    {
        ShowBannerTop();
    }

    void ClearAd()
    {
        CancelInvoke("BannerAd");
        Advertisement.Banner.Hide();
    }
}
