﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using TMPro;

public class PlayerLifeCounter : MonoBehaviour
{
    [SerializeField]
    private int extraLives = 0;

    [SerializeField]
    private int scoreToGainLife = 500;

    int startScoreToGainLife = 0;

    int livesAdded = 1;
    Text livesCount;

    public delegate void ExtraLife();
    public static event ExtraLife OnExtraLife;

    private void Awake()
    {
        livesCount = GameObject.Find("Lives").GetComponent<Text>();
        SetLivesCount();
        startScoreToGainLife = scoreToGainLife;
    }

    void OnEnable()
    {
        PlayerController.OnGameOver += ResetLivesAdded;
        PlayerController.OnResetAfterDeath += SetLivesCount;
        PlayerCollisions.OnEnemyKilled += CheckScore;
        PlayerCollisions.OnMultiplyEnemyKilled += CheckScore;
    }


    void OnDisable()
    {
        PlayerController.OnGameOver -= ResetLivesAdded;
        PlayerController.OnResetAfterDeath -= SetLivesCount;
        PlayerCollisions.OnEnemyKilled -= CheckScore;
        PlayerCollisions.OnMultiplyEnemyKilled -= CheckScore;
    }

    public bool IsGameOver()
    {
        --extraLives;
        if (extraLives < 0)
        {
            scoreToGainLife = startScoreToGainLife;
            //ResetLivesAdded();
            return true;
        }
        else
        {
            return false;
        }
    }

    void AddLife()
    {
        extraLives++;
        livesAdded++;
        SetLivesCount();
        OnExtraLife?.Invoke();
    }

    void CheckScore()
    {
        float scoreTest = ScoreManager.currentScore / (livesAdded * scoreToGainLife);
        if (scoreTest >= 1)
        {
            AddLife();
            scoreToGainLife += (scoreToGainLife / 2);
        }
    }

    void ResetLivesAdded()
    {
        livesAdded = 1;
        extraLives = 0;
    }

    void SetLivesCount()
    {
        livesCount.text = "" + extraLives;
    }
}
