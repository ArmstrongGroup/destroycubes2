using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFeatures : MonoBehaviour
{
    [SerializeField]
    Transform playerT;
    [SerializeField]
    Transform face;
    [SerializeField]
    GameObject aliveFace;
    [SerializeField]
    GameObject deadFace;

    Animator[] anims;

    float lastX = 0;

    // Start is called before the first frame update
    void Start()
    {
        lastX = transform.position.x;
        anims = GetComponentsInChildren<Animator>();
    }

    private void OnEnable()
    {
        PlayerController.OnResetAfterDeath += PlayerAlive;
        GameStateMachine.OnEnterGame += PlayerAlive;

        PlayerController.OnLostLife += PlayerDead;
        PlayerController.OnGameOver += PlayerDead;
    }

    private void OnDisable()
    {
        PlayerController.OnResetAfterDeath -= PlayerAlive;
        GameStateMachine.OnEnterGame -= PlayerAlive;

        PlayerController.OnLostLife -= PlayerDead;
        PlayerController.OnGameOver -= PlayerDead;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        transform.position = playerT.position;

        if (lastX < transform.position.x)
        {
            face.localScale = new Vector3(1, 1, 1);
        }
        else if (lastX > transform.position.x)
        {
            face.localScale = new Vector3(-1, 1, 1);
        }
        lastX = transform.position.x;
    }

    void PlayerAlive() 
    {
        aliveFace.SetActive(true);
        deadFace.SetActive(false);

        foreach (Animator anim in anims) 
        {
            anim.Play("Motion");
        }
    }

    void PlayerDead()
    {
        aliveFace.SetActive(false);
        deadFace.SetActive(true);

        foreach (Animator anim in anims)
        {
            anim.Play("Still");
        }
    }
}