﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FingerFeedback : MonoBehaviour
{
    [SerializeField]
    float markTime = 5;

    [SerializeField]
    GameObject fingerMark;

    [SerializeField]
    Sprite[] fingerMarks;

    GameObject finger;
    Image fingerImage;
    Color startColour;
    Color targetColour;
    bool lerping;

    // Start is called before the first frame update
    void Awake()
    {
        finger = Instantiate(fingerMark);
        finger.transform.SetParent(GameObject.Find("PlayScreen").transform);
        fingerImage = finger.GetComponent<Image>();
        startColour = fingerImage.color;
        targetColour = new Color(startColour.r, startColour.g, startColour.b, 0);
        fingerImage.color = targetColour;
    }

    // Update is called once per frame
    void Update()
    {
        if (lerping)
        {
            LerpColour();
        }
    }

    public void StartFingerFeedback(Vector3 screenPos)
    {
        finger.transform.position = screenPos;
        if (screenPos != Vector3.zero)
        {
            fingerImage.sprite = fingerMarks[Random.Range(0, fingerMarks.Length)];
            fingerImage.color = startColour;
            lerping = true;
        }
    }

    void LerpColour()
    {
        fingerImage.color = Color.Lerp(fingerImage.color, targetColour, markTime * Time.deltaTime);
        if (fingerImage.color.a < 0.01)
        {
            lerping = false;
            fingerImage.color = targetColour;
        }
    }
}
