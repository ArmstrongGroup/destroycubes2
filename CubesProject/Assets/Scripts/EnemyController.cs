﻿using UnityEngine;

public class EnemyController : MonoBehaviour
{
    GameObject _player;
    Transform _visTrans;
    //EnemyParticle _particle;

    //float navUpdateInterval = 0.4f;
    //float nextNavUpdate;

    float speed = 3.5f;
    Collider _col;


    enum EnemyState {attacking, penned, stopped};
    EnemyState currentEnemyState = EnemyState.penned;

    public virtual bool IsPenned()
    {
        if (currentEnemyState == EnemyState.penned)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    void OnEnable()
    {
        PlayerController.OnGameOver += Stopped;
        PlayerController.OnLostLife += Stopped;
        GameStateMachine.OnStartScreen += Killed;
        PlayerController.OnResetAfterDeath += Killed;
    }

    void OnDisable()
    {
        PlayerController.OnGameOver -= Stopped;
        PlayerController.OnLostLife -= Stopped;
        GameStateMachine.OnStartScreen -= Killed;
        PlayerController.OnResetAfterDeath -= Killed;
    }

    // Start is called before the first frame update
    void Start()
    {
        currentEnemyState = EnemyState.penned;
        _player = GameObject.FindGameObjectWithTag("Player");
        _visTrans = transform.GetChild(0).GetComponent<Transform>();
        _col = GetComponent<Collider>();
        //_particle = GetComponent<EnemyParticle>();
    }

    public virtual void EnemyUpdate()
    {
        switch (currentEnemyState)
        {
            case EnemyState.attacking:
                AlternativeMove();
                break;

            case EnemyState.stopped:
                break;

            case EnemyState.penned:
                break;

            default:
                Debug.LogError("Invalid Enemy State");
                break;
        }
    }

    /*void NavUpdate()
    {
        if (Time.time > nextNavUpdate)
        {
            nextNavUpdate = Time.time + navUpdateInterval;
        }
    }*/

    void AlternativeMove()
    {
        Vector3 lookAt = _player.transform.position - transform.position;
        if (lookAt != Vector3.zero)
        {
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(lookAt), 5f);
        }
        transform.position = transform.position + (transform.forward * speed * Time.deltaTime * TimeManager.timeSpeed);
    }

    public virtual bool IsSpawnable()
    {
        if (currentEnemyState == EnemyState.penned)
        {
            return true;
        }
        return false;
    }

    public virtual void Killed()
    {
        //_particle.ShowParticle();
        currentEnemyState = EnemyState.penned;
        _col.enabled = false;
        transform.position = new Vector3(0, 0, 12);
        _visTrans.localRotation = Quaternion.Euler(Vector3.zero);
    }

    public virtual void Destroyed()
    {
        Killed();
    }

    public void EnemySpawn(Vector3 spawnPos, Vector3 spawnScale, float spawnSpeed)
    {
        SetColour();
        speed = spawnSpeed;
        transform.position = spawnPos;
        transform.localScale = spawnScale;
        _col.enabled = true;
        
        BroadcastMessage("Spawned");
        currentEnemyState = EnemyState.attacking;
    }

    void SetColour()
    {
        int dice = Random.Range(0, 3);
        //Debug.Log(dice);
        switch (dice)
        {
            case 0:
                _visTrans.localRotation = Quaternion.Euler(Vector3.zero); 
                break;

            case 1:
                _visTrans.localRotation = Quaternion.Euler(new Vector3(-90, 0, 0));
                break;

            case 2:
                _visTrans.localRotation = Quaternion.Euler(new Vector3(0, 0, -90));
                break;

            default:
                Debug.LogError("Roll " + dice);
                break;
        }
    }

    public virtual void Stopped()
    {
        currentEnemyState = EnemyState.stopped;
    }
}
