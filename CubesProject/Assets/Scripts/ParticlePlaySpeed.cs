using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticlePlaySpeed : MonoBehaviour
{
    ParticleSystem pSys;

    // Start is called before the first frame update
    void Start()
    {
        pSys = GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        if (pSys.isPlaying) 
        {
            var main = pSys.main;
            if (TimeManager.timeSpeed < 1)
            {
                main.simulationSpeed = (TimeManager.timeSpeed + 0.5f);
            }
            else 
            { 
                main.simulationSpeed = 1; 
            }
        }
    }
}
