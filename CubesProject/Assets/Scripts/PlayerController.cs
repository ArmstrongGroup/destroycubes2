﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private float moveSpeed = 1f;
    [SerializeField]
    private float attackSpeed = 1f;
    [SerializeField]
    Texture2D mouseCursor;

    public enum PlayerState {move, attack, frenzy, died, gameOver};
    public PlayerState currentPlayerState = PlayerState.move;

    FingerFeedback _fingerFeedback;

    bool touchStart = false;

    SoundFXManager _sFXManager;

    Vector3 inputTarget;
    Vector3 lastFramePos;
    Vector3 penPosition;
    EnemyController _tController;
    PlayerCollisions _pCollisions;
    FingerCollisions _fCollisions;
    TrailRenderer _tRenderer;
    PlayerLifeCounter _pLCounter;
    PlayerEnergy _pEnergy;
    float frenzyTime;

    bool inPlayMode = false;


    public delegate void GameOver();
    public static event GameOver OnGameOver;

    public delegate void LostLife();
    public static event LostLife OnLostLife;

    public delegate void ResetAfterDeath();
    public static event ResetAfterDeath OnResetAfterDeath;

    public delegate void EnergyLow();
    public static event EnergyLow OnEnergyLow;

    public delegate void MoveEnd();
    public static event MoveEnd OnMoveEnd;

    public delegate void EndFrenzy();
    public static event EndFrenzy OnEndFrenzy;

    private void Awake()
    {
        _fingerFeedback = GetComponent<FingerFeedback>();
        _sFXManager = GameObject.Find("Sound").GetComponent<SoundFXManager>();
    }

    // Start is called before the first frame update
    void Start()
    {
        penPosition = transform.position;
        _fCollisions = GetComponent<FingerCollisions>();
        _pLCounter = GetComponent<PlayerLifeCounter>();
        _pEnergy = GetComponent<PlayerEnergy>();
        _tRenderer = GetComponent<TrailRenderer>();
        _tRenderer.emitting = false;
        CursorSet();
    }

    void CursorSet()
    {
        #if UNITY_EDITOR
        CursorMode cursorMode = CursorMode.Auto;
        Cursor.SetCursor(mouseCursor, Vector2.zero, cursorMode);
        #endif

        #if UNITY_ANDROID

        #endif
    }

    void OnEnable()
    {
        GameStateMachine.OnEnterGame += StartPlayer;
        //GameStateMachine.OnEnterGame += SetInPlayModeTrue;
        GameStateMachine.OnStartScreen += PenPlayer;
        ScoreManager.OnEnterFrenzy += EnterFrenzy;
    }

    void OnDisable()
    {
        GameStateMachine.OnEnterGame -= StartPlayer;
        //GameStateMachine.OnEnterGame -= SetInPlayModeTrue;
        GameStateMachine.OnStartScreen -= PenPlayer;
        ScoreManager.OnEnterFrenzy -= EnterFrenzy;
    }

    void SetInPlayModeTrue()
    {
        _fingerFeedback.StartFingerFeedback(Vector3.zero);
        //Invoke("TouchReady", 0.2f);
        TouchReady();
    }

    void TouchReady()
    {
        inPlayMode = true;
    }

    void SetInPlayModeFalse()
    {
        inPlayMode = false;
        _fingerFeedback.StartFingerFeedback(Vector3.zero);
    }

    bool TargetOnScreen(Vector3 targetPos) 
    {
        if (targetPos.x > 8 || targetPos.x < -8)
        {
            Debug.LogAssertion(targetPos + " The off screen bug occured and was caught");
            return false;
        }
        else if (targetPos.z > 4.5f || targetPos.z < -4.5f) 
        {
            Debug.LogAssertion(targetPos + " The off screen bug occured and was caught");
            return false;
        }
        return true;
    }

    void TargetCheck() 
    {
        if (!TargetOnScreen(inputTarget))
        {
            _tController = null;
            PlayerCollisions.targetEnemy = null;
            StopMove(transform.position);
            currentPlayerState = PlayerState.move;
        }
    }

    // Update is called once per frame
    public void PlayerUpdate()
    {
        if (!PlayerCollisions.targetEnemy || _tController == null)
        {
            _tController = null;
            PlayerCollisions.targetEnemy = null;
        }

       

        switch (currentPlayerState)
        {
            case PlayerState.move:
                TargetCheck();
                Inputs();
                MoveTo();
                break;
            case PlayerState.attack:
                TargetCheck(); 
                Inputs();
                AttackTo();
                break;
            case PlayerState.frenzy:
                TargetCheck(); 
                FrenzyInputs();
                FrenzyTo();
                FrenzyTimer();
                break;
            case PlayerState.died:
                StopMove(transform.position);
                break;
            case PlayerState.gameOver:
                StopMove(transform.position);
                break;
            default:
                Debug.LogError("Invalid Player State");
                break;
        }
    }

    void EnterFrenzy()
    {
        currentPlayerState = PlayerState.frenzy;
        frenzyTime = Time.time + 5;
    }

    void FrenzyTimer()
    {
        if (Time.time > frenzyTime && (transform.position == inputTarget))
        {
            OnEndFrenzy?.Invoke();
            if (PlayerCollisions.targetEnemy == null)
            {
                currentPlayerState = PlayerState.move;
            }
            else
            {
                currentPlayerState = PlayerState.attack;
            }
        }
    }

    void DeathCheck()
    {
        _tRenderer.emitting = false;
        StopMove(transform.position);
        if (_pLCounter.IsGameOver())
        {
            currentPlayerState = PlayerState.gameOver;
            SetInPlayModeFalse();
            OnGameOver?.Invoke();
        }
        else
        {
            currentPlayerState = PlayerState.died;
            SetInPlayModeFalse();
            OnLostLife?.Invoke();
            Invoke("LifeLostRestart", 2);
        }
    }

    void LifeLostRestart()
    {
        StartPlayer();
        OnResetAfterDeath?.Invoke();
    }

    void FrenzyMouseInputs()
    {
        if (Input.GetMouseButtonDown(0) && inPlayMode) //causing bug where player canf
        {
            CancelInvoke();
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            _fingerFeedback.StartFingerFeedback(Input.mousePosition);

            //Clamps can probably go now
            //float x = Mathf.Clamp(mousePos.x, -8f, 8f);
            //float z = Mathf.Clamp(mousePos.z, -4.5f, 4.5f);

            inputTarget = new Vector3(mousePos.x, 0, mousePos.z);
            _tRenderer.emitting = true;

            PlayerCollisions.targetEnemy = _fCollisions.GetPlayerMoveTarget(inputTarget);
            _sFXManager.AttackMove();
            if (!PlayerCollisions.targetEnemy)
            {
                _tController = null;
            }
            else
            {
                _tController = PlayerCollisions.targetEnemy.GetComponent<EnemyController>();
            }
        }
    }

    void FrenzyFingerInputs()
    {
        if (Input.touchCount == 0 && inPlayMode)
        {
            touchStart = false;
        }
        else if (Input.touchCount >= 1 && !touchStart && inPlayMode)
        {
            touchStart = true;
            CancelInvoke();
            Vector3 fingerPos = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
            _fingerFeedback.StartFingerFeedback(Input.GetTouch(0).position);

            //Clamps can probably go now
            //float x = Mathf.Clamp(fingerPos.x, -8f, 8f);
            //float z = Mathf.Clamp(fingerPos.z, -4.5f, 4.5f);

            inputTarget = new Vector3(fingerPos.x, 0, fingerPos.z);
            _tRenderer.emitting = true;

            PlayerCollisions.targetEnemy = _fCollisions.GetPlayerMoveTarget(inputTarget);
            _sFXManager.AttackMove();
            if (!PlayerCollisions.targetEnemy)
            {
                _tController = null;
            }
            else
            {
                _tController = PlayerCollisions.targetEnemy.GetComponent<EnemyController>();
            }
        }
    }

    void Inputs()
    {
        #if UNITY_EDITOR
            PlayerMouseInputs();
        #endif

        #if UNITY_ANDROID
            PlayerFingerInputs();
        #endif
    }

    void FrenzyInputs()
    {
        #if UNITY_EDITOR
            FrenzyMouseInputs();
        #endif

        #if UNITY_ANDROID
            FrenzyFingerInputs();
        #endif
    }

    void PlayerMouseInputs()
    {
        if (Input.GetMouseButtonDown(0) && inPlayMode)
        {
            if (_pEnergy.Energy >= 0)
            {
                CancelInvoke();
                Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                _fingerFeedback.StartFingerFeedback(Input.mousePosition);
                //float x = Mathf.Clamp(mousePos.x, -8f, 8f);
                //float z = Mathf.Clamp(mousePos.z, -4.5f, 4.5f);

                inputTarget = new Vector3(mousePos.x, 0, mousePos.z);

                PlayerCollisions.targetEnemy = _fCollisions.GetPlayerMoveTarget(inputTarget);

                if (!PlayerCollisions.targetEnemy)
                {
                    _sFXManager.RegularMove();
                    _pEnergy.EnergyUsed();
                    _pEnergy.RechargeStart();
                    currentPlayerState = PlayerState.move;
                    _tRenderer.emitting = false;
                }
                else
                {
                    _sFXManager.AttackMove();
                    _pEnergy.RechargeStart();
                    currentPlayerState = PlayerState.attack;
                    _tController = PlayerCollisions.targetEnemy.GetComponent<EnemyController>();
                    _tRenderer.emitting = true;
                }
            }
            else
            {
                _pEnergy.RechargeStart();
                OnEnergyLow?.Invoke();
            }
        }
    }

    void PlayerFingerInputs()
    {
        if (Input.touchCount == 0 && inPlayMode)
        {
            touchStart = false;
        }
        else if (Input.touchCount >= 1 && !touchStart && inPlayMode)
        {
            touchStart = true;
            if (_pEnergy.Energy >= 0)
            {

                CancelInvoke();
                Vector3 fingerPos = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
                _fingerFeedback.StartFingerFeedback(Input.GetTouch(0).position);
                //float x = Mathf.Clamp(fingerPos.x, -8f, 8f);
                //float z = Mathf.Clamp(fingerPos.z, -4.5f, 4.5f);

                inputTarget = new Vector3(fingerPos.x, 0, fingerPos.z);

                //_pEnergy.EnergyUsed();
                //_pEnergy.RechargeStart();

                PlayerCollisions.targetEnemy = _fCollisions.GetPlayerMoveTarget(inputTarget);

                if (!PlayerCollisions.targetEnemy)
                {
                    _sFXManager.RegularMove();
                    _pEnergy.EnergyUsed();
                    _pEnergy.RechargeStart();
                    currentPlayerState = PlayerState.move;
                    _tRenderer.emitting = false;
                }
                else
                {
                    _sFXManager.AttackMove();
                    _pEnergy.RechargeStart();
                    currentPlayerState = PlayerState.attack;
                    _tController = PlayerCollisions.targetEnemy.GetComponent<EnemyController>();
                    _tRenderer.emitting = true;
                }
            }
            else
            {
                _pEnergy.RechargeStart();
                OnEnergyLow?.Invoke();
            }
        }
    }

    void MoveTo()
    {
        float disToGo = Vector3.SqrMagnitude(transform.position - inputTarget);
        float disLastTrav = Vector3.SqrMagnitude(transform.position - lastFramePos);

        if (disToGo > disLastTrav)
        {
            lastFramePos = transform.position;
            transform.LookAt(inputTarget);
            transform.position = transform.position + (transform.forward * moveSpeed * Time.deltaTime * TimeManager.timeSpeed);
        }
        else
        {
            StopMove(inputTarget);
        }
    }

    void FrenzyTo()
    {
        float disToGo;
        if (PlayerCollisions.targetEnemy != null && !_tController.IsPenned())
        {
            disToGo = Vector3.SqrMagnitude(transform.position - PlayerCollisions.targetEnemy.transform.position);
            transform.LookAt(PlayerCollisions.targetEnemy.transform.position);
            inputTarget = PlayerCollisions.targetEnemy.transform.position;
        }
        else
        {
            disToGo = Vector3.SqrMagnitude(transform.position - inputTarget);
            transform.LookAt(inputTarget);
        }

        transform.position = transform.position + (transform.forward * attackSpeed * Time.deltaTime * TimeManager.timeSpeed);

        if (disToGo < 1)
        {
            transform.position = inputTarget;
            OnMoveEnd?.Invoke();
        }

        if (inputTarget.x > 8 || inputTarget.x < -8 || inputTarget.z > 4.5f || inputTarget.z < -4.5f)
        {
            transform.position = inputTarget;
            OnMoveEnd?.Invoke();
        }

        //lastFramePos = transform.position;
    }

    void AttackTo()
    {
        if (PlayerCollisions.targetEnemy == null)
        {
            MoveOutOfAttack();
            return;
        }
        else if (_tController.IsPenned())
        {
            PlayerCollisions.targetEnemy = null;
            MoveOutOfAttack();
            return;
        }
        else if (_tController.transform.position.x > 8 || _tController.transform.position.x < -8 || _tController.transform.position.z > 4.5f || _tController.transform.position.z < -4.5f)
        {
            PlayerCollisions.targetEnemy = null;
            MoveOutOfAttack();
            return;
        }

        lastFramePos = transform.position;
        transform.LookAt(PlayerCollisions.targetEnemy.transform.position);
        transform.position = transform.position + (transform.forward * attackSpeed * Time.deltaTime * TimeManager.timeSpeed);
    }

    void MoveOutOfAttack()
    {
        StopMove(transform.position);
        _tController = null;
        currentPlayerState = PlayerState.move;
    }

    void StopMove(Vector3 stopLoc)
    {
        transform.position = stopLoc;
        inputTarget = stopLoc;
        lastFramePos = transform.position;
    }

    void PenPlayer()
    {
        transform.position = penPosition;
    }

    void StartPlayer()
    {
        Invoke("PlacePlayer", 0.3f);
        SetInPlayModeFalse();
        PenPlayer();
    }

    void PlacePlayer()
    {
        //Debug.Log("Smashing...");
        currentPlayerState = PlayerState.move;
        SetInPlayModeTrue();
        transform.position = Vector3.zero;
        lastFramePos = transform.position;
        inputTarget = Vector3.zero;
    }
}
