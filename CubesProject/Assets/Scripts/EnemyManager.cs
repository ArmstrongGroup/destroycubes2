﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public GameObject enemyPrefab;
    public int numberOfEnemies = 1;
    public float firstSpawnTime = 4;
    public float maxSpawnGap = 4;
    public float minSpawnGap = 0.5f;

    public AnimationCurve spawnPattern;

    public EnemyController[] enemies;

    float nextSpawn;
    float startTime;

    void OnEnable()
    {
        GameStateMachine.OnEnterGame += SetStartTime;
    }

    void OnDisable()
    {
        GameStateMachine.OnEnterGame -= SetStartTime;
    }

    // Start is called before the first frame update
    void Awake()
    {
        Physics.IgnoreLayerCollision(8, 8);
        CreateEnemies();
    }

    void CreateEnemies()
    {
        enemies = new EnemyController[numberOfEnemies];
        for (int i = 0; i < enemies.Length; i++)
        {
            enemies[i] = Instantiate(enemyPrefab).GetComponent<EnemyController>();
            enemies[i].gameObject.transform.position = new Vector3(0, 0, 12);
        }
    }

    void Spawn()
    {
        if (IsNextSpawn())
        {
            for (int i = 0; i < enemies.Length; i++)
            {
                if (enemies[i].IsSpawnable())
                {
                    float speed = Random.Range(3f, 3.5f);
                    float size = Random.Range(0.5f, 1f);
                    Vector3 scale = new Vector3(size, size, size);
                    Vector3 pos = GeneratePos();
                    enemies[i].EnemySpawn(pos, scale, speed);
                    break;
                }
            }
        }
    }

    void SetStartTime()
    {
        startTime = Time.time;
    }

    float GenerateNextSpawnTime()
    {
        float time = spawnPattern.Evaluate(Time.time - startTime) * maxSpawnGap;
        if (time < minSpawnGap) 
        {
            time = minSpawnGap;
        }

        return time;
    }

    Vector3 GeneratePos()
    {
        int side = Random.Range(1, 5);
        Vector3 loc = Vector3.zero;
        switch (side)
        {
            case 1:
                loc = new Vector3(Random.Range(-12, 12), 0, 12);

                break;
            case 2:
                loc = new Vector3(18, 0, Random.Range(-8, 8));

                break;
            case 3:
                loc = new Vector3(Random.Range(-12, 12), 0, -12);

                break;
            case 4:
                loc = new Vector3(-18, 0, Random.Range(-8, 8));

                break;
            default:
                //Should never be seen, but just in case
                loc = new Vector3(-18, 0, Random.Range(-8, 8));
                break;
        }
        return loc;
    }

    // Update is called once per frame
    public void EnemiesUpdate()
    {
        RunEnemies();
        Spawn();
    }

    void RunEnemies()
    {
        foreach (EnemyController enemy in enemies)
        {
            enemy.EnemyUpdate();
        }
    }

    bool IsNextSpawn()
    {
        if (Time.time > nextSpawn && Time.time > startTime + firstSpawnTime)
        {
            nextSpawn = Time.time + GenerateNextSpawnTime();
            return true;
        }
        return false;
    }
}
