﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour
{
    bool slowDown = false;

    public static float timeSpeed = 1;

    void OnEnable()
    {
        PlayerCollisions.OnMultiplyEnemyKilled += StartSlowMo;
        ScoreManager.OnEnterFrenzy += StartSlowMo;
        PlayerController.OnEndFrenzy += StartSlowMo;
        PlayerLifeCounter.OnExtraLife += StartSlowMo;
        PlayerController.OnGameOver += ResetTimeSpeed;
    }

    void OnDisable()
    {
        PlayerCollisions.OnMultiplyEnemyKilled -= StartSlowMo;
        ScoreManager.OnEnterFrenzy -= StartSlowMo;
        PlayerController.OnEndFrenzy -= StartSlowMo;
        PlayerLifeCounter.OnExtraLife -= StartSlowMo;
        PlayerController.OnGameOver -= ResetTimeSpeed;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    public void TimeUpdate()
    {
        SlowMo();
    }

    void SlowMo()
    {
        if (slowDown)
        {
            //CancelInvoke("ResetTimeSpeed");
            //timeSpeed = 0;
            LerpTime();
            //Invoke("ResetTimeSpeed", 0.2f);
            //slowDown = false;
        }
    }

    void ResetTimeSpeed()
    {
        timeSpeed = 1;
    }

    void LerpTime()
    {
        if (timeSpeed < 0.9f)
        {
            float targetTimeScale = Mathf.Lerp(timeSpeed, 1, Time.deltaTime * 10f);
            timeSpeed = targetTimeScale;
        }
        else
        {
            timeSpeed = 1;
            slowDown = false;
        }
    }

    void StartSlowMo()
    {
        timeSpeed = 0;
        slowDown = false;
        CancelInvoke("Paused");
        Invoke("Paused", 0.1f);
        //Time.timeScale = 0.3f;
    }

    void Paused() 
    {
        slowDown = true;
    }
}
