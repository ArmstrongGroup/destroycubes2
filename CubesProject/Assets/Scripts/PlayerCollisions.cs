﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollisions : MonoBehaviour
{
    [SerializeField]
    private LayerMask hitMask;
    [SerializeField]
    private LayerMask hitMaskForEnemy;
    [SerializeField]
    float invicTime = 0.2f;

    Vector3 lastFramePos;
    Vector3 motionVector;
    Vector3 finalPos;

    //bool endAttack;

    float invincTimeEnd = 0;
    PlayerController _pController;

    public delegate void EnemyKilled();
    public static event EnemyKilled OnEnemyKilled;

    public delegate void MultiplyEnemyKilled();
    public static event MultiplyEnemyKilled OnMultiplyEnemyKilled;

    public static GameObject targetEnemy;

    private void Awake()
    {
        _pController = GetComponent<PlayerController>();
    }

    public void PlayerCollisionsUpdate()
    {
        CollisionsStateMachine();
        lastFramePos = transform.position;
        //Debug.Log(targetEnemy);
    }

    void CollisionsStateMachine()
    {
        switch (_pController.currentPlayerState)
        {
            case PlayerController.PlayerState.move:
                if (InvincibleTime())
                {
                    InvincibleCollisions();
                }
                else
                {
                    DeathCollision();
                }
                break;

            case PlayerController.PlayerState.attack:
                if (targetEnemy != null)
                {
                    KillCollisions();
                }
                break;

            case PlayerController.PlayerState.frenzy:
                FrenzyCollisions();
                break;

            case PlayerController.PlayerState.died:
                
                break;

            case PlayerController.PlayerState.gameOver:

                break;

            default:
                Debug.LogError("Invalid Player State");
                break;
        }
    }

    bool InvincibleTime()
    {
        if (Time.time < invincTimeEnd)
        {
            return true;
        }
        return false;
    }

    void SetInvicibleTime()
    {
        
        invincTimeEnd = Time.time + invicTime;
    }

    public void DeathCollision()
    {
        //Debug.Log("Death");
        Collider[] hits;
        hits = Physics.OverlapCapsule(lastFramePos, transform.position, 0.25f, hitMask);

        if (hits.Length > 0)
        {
            if (hits[0].CompareTag("Enemy"))
            {
                SetMotionVector();
                PlacePlayerOnHit(hits[0].gameObject);
                hits[0].gameObject.BroadcastMessage("Stopped");
            }
            BroadcastMessage("DeathCheck");
        }
        //lastFramePos = transform.position;
    }

    //TODO Fix this. Does not work as intended. This is due to while loop not being able to move object 
    //multiple times per frame.
    void PlacePlayerOnHit(GameObject obj)
    {
        if (transform.position == lastFramePos)
        {
            return;
        }
        else if (Vector3.SqrMagnitude(transform.position - lastFramePos) < 0.2f) 
        {
            return;
        }

        transform.position = lastFramePos;
        RaycastHit hit;
        Vector3 dir = obj.transform.position - transform.position;
        Physics.Raycast(transform.position, dir, 12, hitMask);
        if (Physics.Raycast(transform.position, dir, out hit, 2, hitMask))
        {
            transform.position = hit.collider.gameObject.transform.position - (dir.normalized * 0.45f);
        }
        else 
        {
            transform.position = obj.transform.position - (dir.normalized * 0.45f);
        }
    }


    void SetMotionVector()
    {
        motionVector = Vector3.Normalize(transform.position - lastFramePos);
    }

    public void KillCollisions()
    {
        //Debug.Log(targetEnemy.name);
        Collider[] hits;
        hits = Physics.OverlapCapsule(lastFramePos, transform.position, 0.25f, hitMask);

        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i].CompareTag("Enemy"))
            {
                if (hits[i].gameObject == targetEnemy)
                {
                    //hits[i].gameObject.transform.position = new Vector3(0, 0, 12);
                    hits[i].gameObject.BroadcastMessage("Killed");
                    //lastFramePos = transform.position;
                    OnEnemyKilled?.Invoke();
                    SetInvicibleTime();
                    targetEnemy = null;
                }
                else
                {
                    SetMotionVector();
                    PlacePlayerOnHit(hits[i].gameObject);
                    //hits[i].gameObject.transform.position = new Vector3(0, 0, 12);
                    hits[i].gameObject.BroadcastMessage("Killed");
                    //lastFramePos = transform.position;
                    OnMultiplyEnemyKilled?.Invoke();
                }
            }
            else
            {
                if (hits[i].gameObject == targetEnemy)
                {
                    BroadcastMessage("DeathCheck");
                }
                else
                {
                    //Debug.Log(hits[i].gameObject.name);
                    //hits[i].gameObject.BroadcastMessage("Killed");
                }
            }
        }
        //lastFramePos = transform.position;
    }

    void FrenzyCollisions()
    {
        Collider[] hits;
        hits = Physics.OverlapCapsule(lastFramePos, transform.position, 0.25f, hitMask);
        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i].CompareTag("Enemy"))
            {
                if (hits[i].gameObject == targetEnemy)
                {
                    //hits[i].gameObject.transform.position = new Vector3(0, 0, 12);
                    hits[i].gameObject.BroadcastMessage("Killed");
                    //lastFramePos = transform.position;
                    OnEnemyKilled?.Invoke();
                    SetInvicibleTime();
                    targetEnemy = null;
                }
                else
                {
                    SetMotionVector();
                    PlacePlayerOnHit(hits[i].gameObject);

                    //hits[i].gameObject.transform.position = new Vector3(0, 0, 12);
                    hits[i].gameObject.BroadcastMessage("Killed");
                    //lastFramePos = transform.position;
                    OnMultiplyEnemyKilled?.Invoke();
                }
            }
            else
            {
                //hits[i].gameObject.BroadcastMessage("Killed");
            }
        }
        //lastFramePos = transform.position;
    }

    public void InvincibleCollisions()
    {
        //Debug.Log("Pooooooo");
        Collider[] hits;
        hits = Physics.OverlapCapsule(lastFramePos, transform.position, 0.25f, hitMask);

        if (hits.Length > 0)
        {
            for (int i = 0; i < hits.Length; i++)
            {
                if (hits[i].CompareTag("Enemy"))
                {
                    //Debug.Log("Pooooooo2");
                    OnMultiplyEnemyKilled?.Invoke();
                    //hits[i].gameObject.transform.position = new Vector3(0, 0, 12);
                    hits[i].gameObject.BroadcastMessage("Killed");
                }
                else
                {
                    if (hits[i].gameObject == targetEnemy)
                    {
                        BroadcastMessage("DeathCheck");
                    }
                    else
                    {
                        //hits[i].gameObject.BroadcastMessage("Killed");
                    }
                }
            }
        }
        //lastFramePos = transform.position;
    }
}
