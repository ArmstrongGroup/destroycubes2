﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrenzyFire : MonoBehaviour
{
    ParticleSystem _pSystem;
    bool frenzyOn = false;
    Transform playLoc;

    void OnEnable()
    {
        ScoreManager.OnEnterFrenzy += FrenzyStart;
        PlayerController.OnEndFrenzy += FrenzyEnd;
    }

    void OnDisable()
    {
        ScoreManager.OnEnterFrenzy -= FrenzyStart;
        PlayerController.OnEndFrenzy -= FrenzyEnd;
    }

    // Start is called before the first frame update
    void Start()
    {
        _pSystem = GetComponent<ParticleSystem>();
        playLoc = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        if (frenzyOn)
        {
            transform.position = playLoc.position;
        }
    }

    void FrenzyStart()
    {
        _pSystem.Play();
        frenzyOn = true;
    }

    void FrenzyEnd()
    {
        _pSystem.Stop();
        frenzyOn = false;
    }
}
