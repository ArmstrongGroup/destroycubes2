﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MineFlash : MonoBehaviour
{
    [SerializeField]
    float flashTime = 1;
    [SerializeField]
    float flashDuration = 0.2f;
    [SerializeField]
    Color FlashColour = Color.yellow;
    [SerializeField]
    Color BaseColour = Color.red;
    Color targetColour;
    SpriteRenderer sprite;
    float timeTillFlash;
    bool isFlashing;
    float nextFlash;
    SoundFXManager soundFX;

    // Start is called before the first frame update
    void Start()
    {
        sprite = GetComponent<SpriteRenderer>();
        soundFX = GameObject.Find("Sound").GetComponent<SoundFXManager>();
        nextFlash = flashTime;
    }

    // Update is called once per frame
    void Update()
    {
        if (isFlashing)
        {
            Flash();
        }
    }

    void Flash()
    {
        if (Time.time > timeTillFlash)
        {
            sprite.color = FlashColour;
            soundFX.MineBeep();
            timeTillFlash = Time.time + nextFlash;
            nextFlash = nextFlash / 2;
            Invoke("ResetFlash", flashDuration);
        }
        if (TimeManager.timeSpeed == 0)
        {
            timeTillFlash = timeTillFlash + Time.deltaTime;
        }
    }

    private void ResetFlash()
    {
        sprite.color = BaseColour;
    }

    public void BeginFlash() 
    {
        isFlashing = true;
        timeTillFlash = Time.time + nextFlash;
        ResetFlash();
    }

    public void StopFlash()
    {
        isFlashing = false;
        nextFlash = flashTime;
    }
}
