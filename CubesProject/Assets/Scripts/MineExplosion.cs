﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MineExplosion : MonoBehaviour
{
    ParticleSystem _pSystem;
    Animation _anim;

    public bool isPaused;

    void OnEnable()
    {
        GameStateMachine.OnEnterGame += Pen;
        //GameStateMachine.OnEnterGame += StartSpawnTime;
        //GameStateMachine.OnStartScreen += Stop;
        GameStateMachine.OnStartScreen += Play;
        PlayerController.OnResetAfterDeath += Play;
        PlayerController.OnResetAfterDeath += Pen;
        PlayerController.OnGameOver += Pause;
        PlayerController.OnLostLife += Pause;
    }

    void OnDisable()
    {
        GameStateMachine.OnEnterGame -= Pen;
        //GameStateMachine.OnEnterGame += StartSpawnTime;
        //GameStateMachine.OnStartScreen -= Stop;
        GameStateMachine.OnStartScreen -= Play;
        PlayerController.OnResetAfterDeath -= Play;
        PlayerController.OnResetAfterDeath -= Pen;
        PlayerController.OnGameOver -= Pause;
        PlayerController.OnLostLife -= Pause;
    }


    // Start is called before the first frame update
    void Start()
    {
        _pSystem = GetComponent<ParticleSystem>();
        _anim = GetComponent<Animation>();
        Pen();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Play() 
    {
        isPaused = false;
        _pSystem.Play();
        _anim.Play();
    }

    public void Pause()
    {
        _pSystem.Pause();
        _anim.Stop();
        isPaused = true;
    }

    public void Stop()
    {
        _pSystem.Stop();
        _anim.Stop();

    } 

    public void Pen() 
    {
        transform.position = new Vector3(0, 0, 18);
    }
}
