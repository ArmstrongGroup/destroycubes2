﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerEnergy : MonoBehaviour
{
    [SerializeField]
    private Color fullEnergyColour = Color.green;

    [SerializeField]
    private Color midEnergyColour = Color.yellow;

    [SerializeField]
    private Color zeroEnergyColour = Color.red;

    Color currentEnergyColour = Color.red;

    [SerializeField]
    private float energyPerMove = 0.2f;
    //[SerializeField]
    private Slider[] _energySliders;
    Image[] _energyFills;

    private float _energy = 0.5f;
    public float Energy
    {
        get
        {
            return _energy;
        }
    }

    private float _rechargeStart = 0;

    private void OnEnable()
    {
        PlayerCollisions.OnMultiplyEnemyKilled += MultiKillEnergyReGain;
    }
    private void OnDisable()
    {
        PlayerCollisions.OnMultiplyEnemyKilled -= MultiKillEnergyReGain;
    }


    // Start is called before the first frame update
    void Awake()
    {
        _energySliders = new Slider[2];
        _energySliders[0] = GameObject.Find("EnergyBarRight").GetComponent<Slider>();
        _energySliders[1] = GameObject.Find("EnergyBarLeft").GetComponent<Slider>();
        _energyFills = new Image[2];
        _energyFills[0] = GameObject.Find("FillL").GetComponent<Image>();
        _energyFills[1] = GameObject.Find("FillR").GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        EnergyCharge();
        foreach (Slider slider in _energySliders) 
        {
            slider.value = _energy;
        }
        
        EnergyColour();
    }

    public void EnergyCharge()
    {
        if (Time.time >= (_rechargeStart + 0.5f))
        {
            if (_energy < 0.995f)
            {
                _energy = _energy + (1f * Time.deltaTime);
            }
            else
            {
                _energy = 1f;
            }
        }
    }

    public void RechargeStart()
    {
        _rechargeStart = Time.time;
    }

    public void EnergyUsed()
    {
        _energy = _energy - energyPerMove;
    }

    void EnergyColour() 
    {
        if (_energy == 1) 
        {
            currentEnergyColour = fullEnergyColour;
        }
        else if (_energy >= 0.5f)
        {
            currentEnergyColour = Color.Lerp(midEnergyColour, fullEnergyColour, _energy - 0.5f);
        }
        else if (_energy < 0.5f) 
        {
            currentEnergyColour = Color.Lerp(zeroEnergyColour, midEnergyColour, _energy + 0.5f);
        }
        
        foreach (Image im in _energyFills)
        {
            im.color = currentEnergyColour;
        }
    }

    void MultiKillEnergyReGain() 
    {
        if (_energy < 1)
        {
            _energy += 0.1f;
        }
        else if (_energy > 1) 
        {
            _energy = 1;
        }
    }
}
