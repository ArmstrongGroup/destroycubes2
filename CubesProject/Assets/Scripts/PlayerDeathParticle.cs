﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDeathParticle : MonoBehaviour
{
    Transform playerTrans;
    ParticleSystem pSystem;
    Vector3 penLoc;

    void OnEnable()
    {
        
        PlayerController.OnGameOver += SetLoc;
        PlayerController.OnLostLife += SetLoc;
        PlayerController.OnResetAfterDeath += PlayParticle;
        GameStateMachine.OnStartScreen += PlayParticle;
    }

    void OnDisable()
    {
        PlayerController.OnLostLife -= SetLoc;
        PlayerController.OnGameOver -= SetLoc;
        PlayerController.OnResetAfterDeath -= PlayParticle;
        GameStateMachine.OnStartScreen -= PlayParticle;
    }

    // Start is called before the first frame update
    void Start()
    {
        playerTrans = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        pSystem = GetComponent<ParticleSystem>();
        penLoc = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void SetLoc()
    {
        transform.position = playerTrans.position;
    }

    void PlayParticle()
    {
        pSystem.Play();
        Invoke("PenParticle", 1f);
    }

    void PenParticle()
    {
        transform.position = penLoc;
    }
    
}
