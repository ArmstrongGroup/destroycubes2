﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyParticle : MonoBehaviour
{
    [SerializeField]
    GameObject _particlePrefab;

    GameObject player;
    GameObject particle;

    ParticleSystem pSystem;
    Vector3 poolPos = new Vector3(0, 0, -20);

    SoundFXManager soundFX;
    Renderer rend;

    void OnEnable()
    {
        PlayerController.OnLostLife += LocSet;
        PlayerController.OnGameOver += LocSet;
        PlayerController.OnResetAfterDeath += Explode;
        GameStateMachine.OnStartScreen += Explode;
        PlayerController.OnLostLife += PauseParticle;
    }

    void OnDisable()
    {
        PlayerController.OnLostLife -= LocSet;
        PlayerController.OnGameOver -= LocSet;
        PlayerController.OnResetAfterDeath -= Explode;
        GameStateMachine.OnStartScreen -= Explode;
        PlayerController.OnLostLife -= PauseParticle;
    }

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        particle = Instantiate(_particlePrefab, poolPos, Quaternion.Euler(new Vector3(-90,0,0)));
        pSystem = particle.GetComponent<ParticleSystem>();
        soundFX = GameObject.Find("Sound").GetComponent<SoundFXManager>();
        rend = GetComponentInChildren<Renderer>();
    }

    // Update is called once per frame
    public void Killed()
    {
        if (rend.isVisible)
        {
            particle.transform.position = player.transform.position;
            pSystem.Stop();
            pSystem.Play();
            soundFX.EnemyDeath();
        }
    }

    void PauseParticle()
    {
        pSystem.Pause();
    }

    void LocSet()
    {
        particle.transform.position = transform.position;
    }

    void Explode()
    {
        //particle.transform.position = transform.position;
        if (rend.isVisible)
        {
            pSystem.Stop();
            pSystem.Play();
            soundFX.EnemyDeath();
            transform.position = new Vector3(0, 0, 12);
        }
        else if (pSystem.isPaused) 
        {
            pSystem.Play();
            soundFX.EnemyDeath();
            transform.position = new Vector3(0, 0, 12);
        }
    }
}
