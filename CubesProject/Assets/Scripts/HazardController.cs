﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HazardController : EnemyController
{
    [SerializeField]
    float lifeLength = 5f;
    [SerializeField]
    GameObject deathParticlePrefab;
    [SerializeField]
    GameObject explodeParticlePrefab;

    bool spawnable = true;
    MineExplosion mineExplode;
    ParticleSystem deathExplode;
    SoundFXManager soundFX;
    Renderer rend;
    MineFlash flash;
    Collider col;
    //Animator anim;


    void OnEnable()
    {
        GameStateMachine.OnStartScreen += Pool;
        PlayerController.OnGameOver += StopInvoke;
        PlayerController.OnLostLife += StopInvoke;
        PlayerController.OnLostLife += Pause;
        PlayerController.OnResetAfterDeath += Pool;
    }

    void OnDisable()
    {
        GameStateMachine.OnStartScreen -= Pool;
        PlayerController.OnGameOver -= StopInvoke;
        PlayerController.OnLostLife -= StopInvoke;
        PlayerController.OnLostLife -= Pause;
        PlayerController.OnResetAfterDeath -= Pool;
    }

    private void Start()
    {
        mineExplode = Instantiate(explodeParticlePrefab).GetComponent<MineExplosion>();
        deathExplode = Instantiate(deathParticlePrefab).GetComponent<ParticleSystem>();
        mineExplode.transform.position = new Vector3(-12, 0, -12);
        deathExplode.transform.position = new Vector3(-12, 0, -12);
        //mineExplode.Stop();
        soundFX = GameObject.Find("Sound").GetComponent<SoundFXManager>();
        rend = GetComponent<Renderer>();
        flash = GetComponentInChildren<MineFlash>();
        col = GetComponent<Collider>();
        col.enabled = false;
    }

    public override bool IsPenned()
    {
        if (spawnable == true)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public override bool IsSpawnable()
    {
        return spawnable;
    }

    public void Spawn(Vector3 spawnPos)
    {
        if (mineExplode.isPaused || deathExplode.isPaused)
        {
            mineExplode.Stop();
            deathExplode.Stop();
        }
        transform.position = spawnPos;
        flash.BeginFlash();
        spawnable = false;
        col.enabled = true;
        Invoke("Explode", lifeLength);
        Invoke("CheckSeen", 0.1f);
    }

    void StopInvoke()
    {
        CancelInvoke();
        flash.StopFlash();
    }

    void CheckSeen() 
    {
        if (rend.isVisible == false) 
        {
            Pool();
        }
    }

    private void Pool()
    {
        //Debug.Log("Pool");
        spawnable = true;
        col.enabled = false;
        deathExplode.transform.position = transform.position;
        deathExplode.Stop();

        if (rend.isVisible)
        {
            deathExplode.Play();
            soundFX.EnemyDeath();
        }
        StopInvoke();
        transform.position = new Vector3(0, 0, -12);
    }

    public override void Killed()
    {
        Pool();
    }

    void Explode() 
    {
        //Debug.Log("Pool");
        spawnable = true;
        col.enabled = false;
        mineExplode.transform.position = transform.position;
        mineExplode.Stop();

        if (rend.isVisible)
        {
            mineExplode.Play();
            soundFX.HazardDeath();
        }
        flash.StopFlash();
        transform.position = new Vector3(0, 0, -12);
    }

    void Pause()
    {
        mineExplode.Pause();
        deathExplode.Pause();
        flash.StopFlash();
        CancelInvoke();
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag("Enemy"))
        {
            col.gameObject.BroadcastMessage("Destroyed");
            Pool();
            //flash.StopFlash();
        }
        /*else if (col.CompareTag("Player"))
        {
            //col.gameObject.BroadcastMessage("Destroyed");
            Pool();
            flash.StopFlash();
        }*/
    }
}
