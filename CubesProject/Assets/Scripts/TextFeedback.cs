﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using TMPro;

//Provides the player with feedback on their avatars state
//Current used for displaying energy levels
public class TextFeedback : MonoBehaviour
{
    [SerializeField]
    float textTime = 1;

    Text _text;
    RectTransform _rTrans;
    bool randomMotion;

    float lastMotion;
    float nextMotion;


    // Start is called before the first frame update
    void Awake()
    {
        _text = GetComponent<Text>();
        _rTrans = GetComponent<RectTransform>();
    }

    void OnEnable()
    {
        PlayerController.OnGameOver += ClearText;
        PlayerController.OnEnergyLow += EnergyLow;
        ScoreManager.OnEnterFrenzy += FrenzyText;
        PlayerLifeCounter.OnExtraLife += Extralife;
    }


    void OnDisable()
    {
        PlayerController.OnGameOver -= ClearText;
        PlayerController.OnEnergyLow -= EnergyLow;
        ScoreManager.OnEnterFrenzy -= FrenzyText;
        PlayerLifeCounter.OnExtraLife -= Extralife;
    }

    // Update is called once per frame
    void Update()
    {
        RandomMotion();
    }

    // Gitters the text
    void RandomMotion()
    {
        if (randomMotion)
        {
            if (Time.time > nextMotion)
            {
                nextMotion = Time.time + Random.Range(0.05f, 0.09f);
                _text.fontSize = Random.Range(12, 20);
                float rot = Random.Range(-20f, 20f);
                _rTrans.rotation = Quaternion.Euler(new Vector3(0, 0, rot));
            }
        }
    }

    void FrenzyText()
    {
        //randomMotion = true;
        CancelInvoke();

        //Added when random stopped
        _text.fontSize = 12;
        _rTrans.rotation = Quaternion.Euler(Vector3.zero);

        _text.text = "INVINCIBLE!";
        Invoke("ClearText", textTime);
    }

    void ClearText()
    {
        randomMotion = false;
        _text.text = null;
    }

    void EnergyLow()
    {
        //Debug.Log("Stufffffs");
        CancelInvoke();
        _text.fontSize = 12;
        _rTrans.rotation = Quaternion.Euler(Vector3.zero);
        _text.text = "Energy Low!";
        Invoke("ClearText", textTime);
    }

    void Extralife()
    {
        CancelInvoke();
        _text.fontSize = 12;
        _rTrans.rotation = Quaternion.Euler(Vector3.zero);
        _text.text = "Extra Life!";
        Invoke("ClearText", textTime);
    }
}
