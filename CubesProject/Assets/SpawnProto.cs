using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnProto : MonoBehaviour
{
    int spawnCount = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        SpawnDecider();

        if (spawnCount < 11)
        {
            spawnCount++;
        }
    }
     void SpawnDecider() 
     { 
        switch (spawnCount) 
        { 
            case 0:
                BaseSpawn();
                break;
            case 1:
                PatrolSpawn();
                break;

            case 2:
                BaseSpawn();
                break;
            case 3:
                print("2 Together:");
                BaseSpawn();
                PatrolSpawn();
                break;
            case 4:
                print("Base or Patrol:");
                BaseOrPatrol();
                break;
            case 5:
                TrakerSpawn();
                break;
            case 6:
                print("Base or Patrol:");
                BaseOrPatrol();
                break;
            case 7:
                print("2 Together:");
                BaseSpawn();
                TrakerSpawn();
                break;
            case 8:
                print("Base or Patrol:");
                BaseOrPatrol();
                break;
            case 9:
                print("2 Together:");
                PatrolSpawn();
                TrakerSpawn();
                break;
            case 10:
                print("Base or Patrol or Tracker:");
                BaseOrPatrolOrTracker();
                break;
            case 11:
                print("3 Together:");
                PatrolSpawn();
                TrakerSpawn();
                BaseSpawn();
                break;
            default:
                print("Incorrect Spawn Count.");
                break;
        }
     }

    void BaseOrPatrol() 
    {
        if(Random.Range(1, 3) > 1)
        {
            BaseSpawn();
        }
        else
        {
            PatrolSpawn();
        }
    }

    void BaseOrPatrolOrTracker()
    {
        int dice = Random.Range(1, 4);
        if (dice == 1)
        {
            BaseSpawn();
        }
        else if (dice == 2)
        {
            PatrolSpawn();
        }
        else 
        {
            TrakerSpawn();
        }
    }

    void BaseSpawn() 
    {
        print("Base Spawn");
    }
    void PatrolSpawn() 
    {
        print("Patrol Spawn");
    }
    void TrakerSpawn() 
    {
        print("Traker Spawn");
    }

}
